package model.logic;

import java.awt.BorderLayout;

import javax.swing.JFrame;

import com.teamdev.jxmaps.*;
import com.teamdev.jxmaps.swing.MapView;

import controller.Controller;

public class Mapa extends MapView{

	private Map map;

	public Mapa(String nName) {
		double longMax =  -74.062707;
		double longMin =  -74.094723;
		double longMedia = (longMax + longMin)/2;
		double latMax = 4.621360;
		double latMin = 4.597714;
		double latMedia = (latMax + latMin)/2;
		JFrame frame = new JFrame();
		setOnMapReadyHandler(new MapReadyHandler() {

			@Override
			public void onMapReady(MapStatus status) {
				if(status == MapStatus.MAP_STATUS_OK) {					
					map = getMap();

					MapOptions mapOptions = new MapOptions();
					MapTypeControlOptions controlOptions = new MapTypeControlOptions();
					mapOptions.setMapTypeControlOptions(controlOptions);

					map.setOptions(mapOptions);
					map.setCenter(new LatLng(latMedia, longMedia));
					map.setZoom(11.9);

					Circle circle = new Circle(map);
					circle.setCenter(map.getCenter());
					circle.setRadius(2);

					CircleOptions co = new CircleOptions();
					co.setFillColor("aff000");
					co.setFillOpacity(0.35);
					circle.setOptions(co);
				}


			}
		});


		frame.add(this, BorderLayout.CENTER);
		frame.setSize(700, 500);
		frame.setVisible(true);
	}

	public static void main(String[] args) throws Exception 
	{
		Mapa map = new Mapa("Ventana");
	}
}
