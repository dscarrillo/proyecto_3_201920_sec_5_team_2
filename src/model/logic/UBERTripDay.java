package model.logic;

public class UBERTripDay extends UBERTrip{

	private double dow;
	public UBERTripDay(double pSourceid, double pDstid, double pDow, double pMeanTravelTime, double pStandardDeviationTravelTime, double pGeometricMeanTravelTime, double pGeometricStandardDeviationTravelTime) {
		super(pSourceid, pDstid, pMeanTravelTime, pStandardDeviationTravelTime, pGeometricMeanTravelTime,
				pGeometricStandardDeviationTravelTime);
		dow = pDow;
	}
	public double getDow() {
		return dow;
	}

	
}