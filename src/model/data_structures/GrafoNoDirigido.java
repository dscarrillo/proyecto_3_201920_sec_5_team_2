package model.data_structures;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

public class GrafoNoDirigido <K, V>{

	
	//Almacernarlo con listas de adyacencia
	
	private int V;
	private ArrayList<Bag<Integer>> adj;
	private ArrayList vertices;
	private ArrayList check;
	private int[] id;
	public GrafoNoDirigido ()
	{
		V=0;
		adj = new ArrayList<>();
	}
	public GrafoNoDirigido(int n) {
		// TODO Auto-generated constructor stub
	}
	@SuppressWarnings("unchecked")
	public void Graph(int numV){
		this.V = numV;
		adj = new ArrayList<>();
		vertices=new ArrayList<>();
		check = new ArrayList<>();
		for(int v = 0;v < numV; v++) {
			adj.add(new Bag <Integer>());
			getVertices().add( (V) new Vertex<V, K>(v, null));
			check.add(false);
		}
		
	}
	public int id() {
		return id[V()];
	}
	/**
	 * N�mero de v�rtices
	 * @return
	 */
	public int V(){
		return V;
	}
	/**
	 * N�mero de arcos.
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public int E(){
		int rta=0;
		Iterator<K> aux;
		for(int i=0;i<V;i++)
		{
			for(int j=0;j<adj.get(i).size();j++)
			{
				aux=(Iterator<K>) adj.get(i).iterator();
				if((Integer)aux.next()>(i)) {
					rta++;
					
				}
			}
		}
		return rta;
	}
	
	/**
	 * Modifica la informaci�n del v�rtice idVertex.
	 * @param idVertex
	 * @param infoVertex
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void setInfoVertex(K idVertex, V infoVertex){
		for(int i=0;i<V;i++) {
			if(((Vertex) getVertices().get(i)).getId()==(int)idVertex)
			{
				 ((Vertex) getVertices().get(i)).setInfo(infoVertex);
			}
		}
	}
	/**
	 * Obtiene el costo de un arco, si el arco no existe, retorna -1
	 * @param j
	 * @param k
	 * @return
	 */
	public double[] getCostArc(int j, int k){

		double[] rta= new double [3];
		for(int i=0;i<V;i++) {
			if(((Vertex) getVertices().get(i)).getId()==(int)j)
			{
				rta= adj.get(i).getCost((int) k);
			}
		}
		
		return rta;
	}
	/**
	 * Modifica el costo del arco entre los v�rtices idVertexIni e idVertexFin
	 * @param idVertexIni
	 * @param idVertexFin
	 * @param cost
	 */
	public void setCostArc(K idVertexIni, K idVertexFin, double dist, double time, double vel){
		for(int i=0;i<V;i++) {
			if(((Vertex) getVertices().get(i)).getId()==(int)idVertexIni)
			{
			  adj.get(i).setCost((int) idVertexFin, dist, time, vel);
			}
		}
	}
	/**
	 * 
	 * @param i
	 * @param info
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void addVertex(int i, String info){
		if(vertices==null) vertices=new ArrayList<>();
		getVertices().add(new Vertex((int) i,info));
		adj.add(new Bag<>());
		V++;
	}
	
	@SuppressWarnings("unchecked")
	public Iterable <K> adj(int j){
		ArrayList rta = new ArrayList<>();
		for(int i = 0;i < V ;i++)
		{
			if(((Vertex) getVertices().get(i)).getId()==(int)j)
			{
				Iterator aux = adj.get(i).iterator();
				while(aux.hasNext())
				{
					rta.add(aux.next());
				}
			}
			
		}
		return rta;
	}
	
	@SuppressWarnings("unchecked")
	public void uncheck(){
		check=new ArrayList<>();
		for(int i=0;i<V;i++)
		{
			check.add(false);
		}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void dfs(K s){
		Vertex v = (Vertex) s;
		check.add( v.getId(), true);
		for (K w : adj(v.getId()))
			if ((boolean)check.get((int) getVertices().get((int) s)) == false)
			{
				addEdge( (int)w, (int)s, getCostArc((int)w,(int) s)[0], getCostArc((int)w, (int)s)[1], getCostArc((int)w,(int) s)[2]);
				dfs(w);
			}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public int cc(){
		int count = 0;
		id = new int[V()];
		for (int s = 0; s < V(); s++) {
			
			Vertex v = (Vertex) getVertices().get(s);
			v.setId(s); v.setInfo(s);
			if ( (boolean)check.get(v.getId()) == false)
			{
				dfs((K)getVertices().get(s));
				id[s] = count;
				count++;
			}
		}

		return count;
	}
	
	@SuppressWarnings("unchecked")
	public Iterable <K> getCC(K idVertex){
		cc();
		ArrayList<K> cs = new ArrayList<>();
		for(int i = 0; i < V(); i++) {
			if(id[i] == id[(int) idVertex])
			{
				cs.add((K) getVertices().get(i));
			}
		}
		return cs;
	}
	public ArrayList getVertices() {
		return vertices;
	}
	public void setVertices(ArrayList vertices) {
		this.vertices = vertices;
	}
	public void addEdge(int idVertexIni, int idVertexFin, double dist, double time, double vel) {
		// TODO Auto-generated method stub
		boolean existeini=false;
		int id1=0;
		int id2=0;
		boolean existefin=false;
		for(int i=0;i<V;i++) {
			if(((Vertex) getVertices().get(i)).getId()==(int)idVertexIni)
			{
				existeini=true;
				id1=i;
			}
			if(((Vertex) getVertices().get(i)).getId()==(int)idVertexFin)
			{
				existefin=true;
				id2=i;
			}
		}
		if(existeini && existefin)
		{
			adj.get(id1).add((int)idVertexFin, dist, time, vel);
			adj.get(id2).add((int)idVertexIni, dist, time, vel);
		}
	}
	public String getInfoVertex(int idVertex) {
		// TODO Auto-generated method stub
		for(int i=0;i<V;i++) {
			if(((Vertex) getVertices().get(i)).getId()==(int)idVertex)
			{
				return (String) ((Vertex) getVertices().get(i)).getInfo();
			}

		}
		return null;
	}
	public int buscarVerticeLocalizacion(double lat, double lon)
	{
		String info="";
		int ve=0;
		double aux=Double.POSITIVE_INFINITY;
		for(int i=0;i<V;i++)
		{
			info=(String) ((Vertex) vertices.get(i)).getInfo();
			String [] b=info.split(",");
			if((Math.abs(Double.parseDouble(b[0])-lon)+Math.abs(Double.parseDouble(b[1])-lat))<aux)
			{
				ve=i;
				aux=(Math.abs(Double.parseDouble(b[0])-lon)+Math.abs(Double.parseDouble(b[1])-lat));
			}
		}
		return ve;
		
	}
	public Iterable <K> adjNode(int j){
		ArrayList rta = new ArrayList<>();
		for(int i = 0;i < V ;i++)
		{
			if(((Vertex) getVertices().get(i)).getId()==(int)j)
			{
				 Node aux = adj.get(i).first;
				 rta.add(aux);
				while(aux.next!=null)
				{
					rta.add(aux.next);
				}
			}
			
		}
		return rta;
	}
	public ArrayList<Vertex> darVerticesMenosVelocidad(int n)
	{
		for(int k=0;k<adj.size();k++) adj.get(k).velProm();
		double min=Double.POSITIVE_INFINITY;
		ArrayList rta=new ArrayList();
		int j=0;
		int done=0;
		boolean[] mark= new boolean[adj.size()];
		Arrays.fill(mark,Boolean.FALSE);
		while(done<=n)
		{
			for(int i=0; i<adj.size();i++)
			{
				if(adj.get(i).velprom<=min && mark[i]==false)
				{
					min=adj.get(i).velprom;
					j=i;
					
				}
			}
			rta.add(adj.get(j));
			mark[j]=true;
			done++;
			min=Double.POSITIVE_INFINITY;
		}
		return rta;
	}
	
	public int darMenorHaversineEntreVertices(int a, int b) {
		int r = 0;
		int menor = 99999;
		for(int i = 0; i < V(); i++) {
			if(Integer.parseInt(getInfoVertex(a)) == Integer.parseInt(getInfoVertex(b)) && Integer.parseInt(getInfoVertex(a)) < menor) {
				r = Integer.parseInt(getInfoVertex(a));
				menor = r;
			}
			
		}
		return r;
	}
}
